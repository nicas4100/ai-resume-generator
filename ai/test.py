from openai import OpenAI
from dotenv import load_dotenv
import os

# Load the environment variables
load_dotenv()

# Create an instance of the OpenAI class
client = OpenAI()

# Test the completion endpoint
chat_completion = client.chat.completions.create(
    model="gpt-3.5-turbo",
    messages=[{"role": "user", "content": "Hello world"}]
)

print(chat_completion.choices[0].message.content)