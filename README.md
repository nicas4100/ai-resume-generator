# Ai Cover Letter generator

## 

## Description
This project will help you generate your cover letter, only by provding the job description and you resume and let AI do the rest.

A typical cover letter generation will be about 1500 tokens to ChatGPT api, depending on the length of your resume and the job description. 

The cost for 1M tokens is around 0.5 USD, so a resume will cost about 0,00075 USD.


## Installation
You will need a openai api Key, and a credit card linked to the openai account..

The api key can be created [here](https://platform.openai.com/api-keys)

And the billing info can be entered [here](https://platform.openai.com/account/billing/overview)


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
